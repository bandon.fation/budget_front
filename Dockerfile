FROM ubuntu:18.04
RUN apt-get update -y
RUN apt-get install nodejs npm nginx curl wget -y

# RUN npm i -g n 
# RUN n stable 
# RUN npm i -g @babel/cli 
# RUN npm i -g webpack 
# RUN npm i -g webpack-dev-server
# RUN npm i -g webpack-cli
WORKDIR /opt/budget_front
ADD . /opt/budget_front