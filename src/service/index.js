import Eos from 'eosforcejs'
import store from '../data/store'
import axios from 'axios'

const get_http_endpoint = (OPTIONS) => {
  for(let key in OPTIONS){
    console.log(key);
    let {network} = OPTIONS[key];
    OPTIONS[key].httpEndpoint = `${ network.protocol }://${ network.host }:${ network.port }`
  }
}
const OPTIONS = {
  devlopment: {
    network: {
        protocol: 'http',
        blockchain: 'eos',
        host: '192.168.1.146',
        port: 8025,
        chainId: 'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b',
    },
    httpEndpoint: "OPTIONS.get_http_endpoint('devlopment', OPTIONS)", //`${ network.protocol }://${ network.host }:${ network.port }`,
    app_host: 'http://0.0.0.0:32775'
  },
  production: {
    network: {
         protocol: 'https',
         blockchain: 'eos',
         host: 'w3.eosforce.cn',
         port: 443,
         chainId: 'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b',
     },
    httpEndpoint: "OPTIONS.get_http_endpoint('production', OPTIONS)", //`${ network.protocol }://${ network.host }:${ network.port }`,
    app_host: 'https://u.eosforce.io'
  }
}

get_http_endpoint(OPTIONS)


// const env = 'devlopment' ;
const env = 'production' ;
const {network, httpEndpoint, app_host} = OPTIONS[env]
console.log(httpEndpoint);
// const network = {
//     protocol: 'https',
//     blockchain: 'eos',
//     host: 'w3.eosforce.cn',
//     port: 443,
//     chainId: 'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b',
// }
// const httpEndpoint = `${ network.protocol }://${ network.host }:${ network.port }`
// const app_host = 'https://u.eosforce.io'

// const network = {
//     protocol: 'http',
//     blockchain: 'eos',
//     host: '192.168.1.146',
//     port: 8025,
//     chainId: 'bd61ae3a031e8ef2f97ee3b0e62776d6d30d4833c8f7c1645c657b149151004b',
// }
// const httpEndpoint = `${ network.protocol }://${ network.host }:${ network.port }`
// const app_host = 'http://0.0.0.0:32775'

/*
  @params
    page int
    status string
    head_block_num int

  @returns
    rows string[],
    more boolean
*/
export const query_motion_from_app = async (page = 1, status = '', head_block_num) => {
    let url = app_host + '/web/get_motions.do';
    let data = await axios.post(url, {page, status, head_block_num});
    return data.data;
}

export const get_total_reward = async () => {
  let url = app_host + '/web/get_total_reward.do';
  let data = await axios.get(url);
  return data.data;
}

export const get_memo_by_motion = async (motion_id) => {
  let url = app_host + '/web/get_memo_by_motion.do';
  let data = await axios.post(url, {motion_id});
  return data.data;
}

export const log_out = async () => {
  await scatter.forgetIdentity();
  store.commit('set_has_scatter', 1);
  store.commit('set_scatter_instance', {
    auth: null,
    options: null,
    eos: null
  });
}

export const call_scatter = async () => {
  const scatter = window.scatter;
  let identity = await scatter.getIdentity({accounts:[network]}).catch(err => {
    throw err;
  });
  const account = identity.accounts.find(function(acc){
       return acc.blockchain === 'eos';
  });

  let options = {
   authorization: account.name+'@'+account.authority,
   broadcast: true,
   sign: true
  }    

  let auth = {
      actor: account.name,
      permission: account.authority
  }

  let auth_str = `${account.name}@${account.authority}`

  let eos = scatter.eos(network, Eos,  options, "https");
  // let ct = await eos.contract('eosio');
  let result = {
    eos,
    options,
    auth,
    auth_str
  };

  store.commit('set_has_scatter', 2);
  store.commit('set_scatter_instance', result);
  return result;
}

export const get_pledge = async (account_name) => {
  let res = await Eos({httpEndpoint}).getTableRows({
    'code': 'eosio.pledge',
    'table': 'pledge',
    'scope': account_name,
    'limit': 10,
    'json': true
  });
  let left = res.rows.length ? res.rows[0].pledge : 0;
  left = parseFloat(left);
  return left;
}

export const get_available = async (account_name) => {
  let pramas = {
    scope: 'eosio',
    code: 'eosio',
    table: 'accounts',
    lower_bound: account_name,
    json: true,
    limit: 1
  }
  let res = await Eos({httpEndpoint}).getTableRows(pramas);
  let account = res.rows.find(i => i.name == account_name)
  if(!account) return 0;
  return parseFloat(account.available)
}



export const get_head_block_num = async () => {
  let info = await Eos({httpEndpoint}).getInfo({});
  return info;
}

export const open_and_plege = async () => {

  let scatter_result = await call_scatter();
  let {auth, options, auth_str} = scatter_result;
  let eos = scatter.eos(network, Eos,  options, "http");

  await eos.transaction(['eosio.pledge', 'eosio'], tr => {
    tr.eosio_pledge.open("block.out", auth.actor, '', options);
    tr.eosio.transfer(auth.actor, 'eosio.pledge', '101.0000 EOS' ,"block.out", options);
  });

}

export const publish = async ({proposer = '', title = '', content = '', quantity = '0.0000 EOS', end_num = ''}) => {
  quantity = parseFloat(quantity);
  quantity += 0.0000001
  quantity = quantity.toFixed(4);
  quantity = quantity + ' EOS';

  let scatter_result = await call_scatter();
  let {auth, options, auth_str} = scatter_result;

  let account_name = auth.actor;

  let eos = scatter.eos(network, Eos,  options, "http");
  let token = await eos.contract('eosc.budget');
  let result = await token.propose(account_name, title, content, quantity, options);
  return result;
}

export const approve = async ({approver, id, memo = ''}) => {
  let scatter_result = await call_scatter();
  let {auth, options, auth_str} = scatter_result;

  let account_name = auth.actor;

  let eos = scatter.eos(network, Eos, options, 'http');
  let token = await eos.contract('eosc.budget');
  let result = await token.approve(approver, id, memo, options);
  return result;
}

export const unapprove = async ({approver, id, memo = ''}) => {
  let scatter_result = await call_scatter();
  let {auth, options, auth_str} = scatter_result;

  let account_name = auth.actor;

  let eos = scatter.eos(network, Eos, options, 'http');
  let token = await eos.contract('eosc.budget');
  let result = await token.unapprove(approver, id, memo, options);
  return result;
}

export const takecoin = async ({proposer, montion_id, content, quantity}) => {
  let scatter_result = await call_scatter();
  let {auth, options, auth_str} = scatter_result;

  let account_name = auth.actor;

  let eos = scatter.eos(network, Eos, options, 'http');
  let token = await eos.contract('eosc.budget');
  let result = await token.takecoin(proposer, montion_id, content, quantity);
  return result;
}

export const query_motions = async (lower_bound = 0, limit = 10) => {
  // motions
  let res = await Eos({httpEndpoint}).getTableRows({
    'code': 'eosc.budget',
    'table': 'motions',
    'scope': 'eosc.budget',
    limit,
    'json': true,
    lower_bound
  });
  return res;
}

// query_motions();

export const query_committee = async () => {
  // committee
  let res = await Eos({httpEndpoint}).getTableRows({
    'code': 'eosc.budget',
    'table': 'committee',
    'scope': 'eosc.budget',
    'limit': 10,
    'json': true
  });
  return res;
}

export const query_takecoins = async ({scope, limit = 10}) => {
  let res = await Eos({httpEndpoint}).getTableRows({
    'code': 'eosc.budget',
    'table': 'takecoins',
     scope,
     limit,
    'json': true
  });
  return res;
}

// takecoins();

export const query_approvers = async (account_name, lower_bound) => {
  let res = await Eos({httpEndpoint}).getTableRows({
    'code': 'eosc.budget',
    'table': 'approvers',
    'scope':  account_name,
    'limit': 1,
    'json': true,
    'lower_bound': lower_bound
  });
  return res;
}

// approvers

// query_approvers('1kkk')



