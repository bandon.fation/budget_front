import Vuex from 'vuex'
import Vue from 'vue'
import ScatterJS from 'scatterjs-core'
import ScatterEOS from 'scatterjs-plugin-eosjs'
import Eos from 'eosforcejs'
// import Eos from 'eosforce'
import scatter_configs from './scatter_configs'
import account_info from './account_info'

import {
  call_scatter
} from '../service'

Vue.use(Vuex);
ScatterJS.plugins( new ScatterEOS() );
const store = new Vuex.Store({
  modules: {
    account_info,
    scatter_configs
  }
});

store.dispatch('check_is_mobile');
store.dispatch('query_motions');
store.dispatch('get_head_block_num');
store.dispatch('query_reward');
store.dispatch('query_motions_circle');

window.addEventListener('resize', () => {
    store.dispatch('check_is_mobile');
});

document.addEventListener('scatterLoaded', scatterExtension => {
    store.dispatch('check_scatter');
});

const init_scatter = async () => {
  let connected = await ScatterJS.scatter.connect("eosc_budget");
  if(!connected) {
    store.commit('set_has_scatter', -1);
    return ;
  };
  window.connected = connected;
  const scatter = ScatterJS.scatter;
  window.scatter = scatter;
  call_scatter();
  store.dispatch('check_scatter');
}

setTimeout(() => {
  init_scatter();
}, 3000);

export default store;