import {
  publish,
  open_and_plege,
  call_scatter
} from '../service'

export default {
    state: {
        is_mobile: false,
        has_scatter: 0, //[-1, 0, 1], 0 未确认; -1 未安装; 1 已安装; 2 已登录
        scatter_instance: {
          eos: null,
          options: null,
          auth: null
        }
    },
    mutations: {
        set_is_mobile (state, status = false) {
          state.is_mobile = status;
        },
        set_has_scatter (state, status = 1) {
          state.has_scatter = status;
        },
        set_scatter_instance(state, {eos = null, options = null, auth = null}) {
          state.scatter_instance.options = options;
          state.scatter_instance.auth = auth
        }
    },
    actions: {
        check_is_mobile ({commit}) {
          let width = document.body ? document.body.offsetWidth : window.innerWidth;
          if(width < 800){
            commit('set_is_mobile', true);
          }else{
            commit('set_is_mobile', false);
          }
        },
        check_scatter ({ commit, dispatch, getters }) {
          if(window.scatter) {
            commit('set_has_scatter', 1);
          }else{
            commit('set_has_scatter', -1);
          }
        },
        async call_scatter({commit, dispatch, getters}) {
          let res = await call_scatter();
          commit('set_scatter_instance', res);
        }
    },
    getters: {
      account_name (state) {
        return state.scatter_instance.auth ? state.scatter_instance.auth.actor : null;
      }
    }
}