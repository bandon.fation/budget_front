
import {
  get_pledge,
  query_motions,
  query_motion_from_app,
  get_head_block_num,
  query_committee,
  get_total_reward,
  get_available
} from '../service'

export default {
    state: {
      available: 0,
      head_block_num: 0,
      head_block_time: new Date(),
      total_reward: {
        on_load: false,
        total: 0
      },
      committee: {
        rows: [],
        more: true,
        on_load: false
      },
      motions: {
        rows: [],
        more: true,
        on_load: false,
        lower_bound: 0,
        step: 10,
        page: 1,
        status: '',
        status_list:  [
          {text: '全部', status: ''},
          {text: '审批中', status: 1},
          {text: '公示中', status: 2},
          {text: '已通过', status: 3},
          // {text: '已完成', status: 4},
        ]
      }
    },
    mutations: {
        set_available (state, available){
          state.available = available;
        },
        update_load_status (state, {key = '', value = ''}) {
          state[key]['on_load'] = value;
        },
        update_committee (state, {rows = ''}) {
          state.committee.rows.splice(0, state.committee.rows.length, ...rows);
        },
        update_rows (state, {key = '', rows = [], more = false, page = 1, from_top = false}){

          let pre_rows = JSON.parse( JSON.stringify(state[key].rows) );

          rows.forEach(row => {
            let index = pre_rows.findIndex(i => i.motion_id == row.motion_id);
            if(index > -1){
              pre_rows[index] = row;
            }else{
              pre_rows.push(row);
            }
          });

          state[key].rows.splice(0, state[key].rows.length, ...pre_rows);

          state[key].rows.sort((cur, pre) => {
            return pre.approve_end_block_num - cur.approve_end_block_num;
          });

          if(!from_top){
            state[key].page += 1;
            state[key].more = more;
          }
        },
        reset_motions (state, status = '') {
          state.motions.rows.splice(0, state.motions.rows.length);
          state.motions.on_load = false;
          state.motions.page = 1;
          state.motions.status = status;
        },
        set_head_block_num (state, {head_block_num, head_block_time}) {
          state.head_block_num = head_block_num;
          state.head_block_time = new Date(head_block_time);
        },
        update_total_reward (state, total) {
          state.total_reward.total = total;
        }
    },
    actions: {
      async check_plege ({commit, getters}) {
        let account_name = getters['account_name'];
        let pldege = await get_pledge(account_name);
        commit('set_available', pldege);
      },
      async query_motions ({commit, getters, state}, from_top = false) {
        let _key = 'motions';
        if(state[_key].on_load) return ;
        !from_top ? commit('update_load_status', {key: _key, value: true}) : '';
        let page = from_top ? 1 : state[_key].page;
        let head_block_num = getters.head_block_num;
        if(!head_block_num){
          let info = await get_head_block_num();
          head_block_num = info.head_block_num;
        }
        let motions = await query_motion_from_app(page, state[_key].status, head_block_num);
        commit('update_rows', {key: _key, ...motions, from_top});
        !from_top ? commit('update_load_status', {key: _key, value: false}) : '';
      },
      async query_motions_circle ({dispatch}) {
        dispatch('query_motions', true);
        setTimeout(() => {
          dispatch('query_motions_circle');
        }, 10000);
      },
      async get_head_block_num ({commit, dispatch}) {
        let info = await get_head_block_num();
        commit('set_head_block_num', info);
        setTimeout(() => {
          dispatch('get_head_block_num');
        }, 6000);
      },
      async query_committee ({commit, getters, state}) {
        let _key = 'committee';
        if(state[_key].on_load) return;
        commit('update_load_status', {key: _key, value: true});
        let committee = await query_committee();
        commit('update_committee', committee);
        commit('update_load_status', {key: _key, value: false});
      },
      async query_reward ({commit, getters, state}) {
        let _key = 'total_reward';
        if(state[_key].on_load) return;
        commit('update_load_status', {key: _key, value: true});
        let data = await get_total_reward();
        commit('update_total_reward', data.total);
        commit('update_load_status', {key: _key, value: false});
      }
    },
    getters: {
      head_block_num (state) {
        return state.head_block_num;
      },
      committee (state) {
        return state.committee.rows[0];
      }
    }
}