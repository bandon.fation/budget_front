import vue from 'vue'
import vue_router from 'vue-router'
import Vuex from 'vuex'
vue.use(vue_router);
vue.use(Vuex);

import { check_lang_key } from '../lang'
const url_lang = check_lang_key();

import store from '../data/store.js'
import index_view from '../views/index.vue'

const router_link = [
    {
      path: '/',
      redirect: '/' + url_lang,
    },
    {
        path: '/' + url_lang,
        name: 'app',
        component: index_view,
        children: [
            {
                path: '/' + url_lang + '/publish' ,
                name: 'publish',
                component: resolve => require(['../views/publish_form.vue'], resolve),
            },
        ],
    },
    {
        path: '/' + url_lang + '/motion_item/:motion_id' ,
        name: 'motion_item',
        component: resolve => require(['../views/motion_item_view.vue'], resolve),
    }
];

const router_config = new vue_router({
    routes: router_link
});

vue.directive('main-scroll-forbidden', {
  inserted (el, binding, vnode) {
    document.documentElement.style.cssText ="overflow:hidden";
    let l = document.getElementsByClassName('list_coutomer')[0] || null;
    l ? l.style.position = 'relative' : '';
    l ? l.style.zIndex = '2' : '';
    let r = document.getElementsByClassName('right_main_container')[0] || null;
    r ? r.style.position = 'relative' : '';
    r ? r.style.zIndex = '2' : '';
  },
  unbind () {
    document.documentElement.style.cssText ="overflow:auto";

    let l = document.getElementsByClassName('list_coutomer')[0] || null;
    l ? l.style.position = '' : '';
    l ? l.style.zIndex = '' : '';
    let r = document.getElementsByClassName('right_main_container')[0] || null;
    r ? r.style.position = '' : '';
    r ? r.style.zIndex = '' : '';
  }
});

const app = new vue({
    router: router_config,
    store
}).$mount('#app');